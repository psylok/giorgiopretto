const CleanCSS = require("clean-css");

module.exports = function(config) {
  // Module to minify and inline CSS
  config.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  });

  config.addPassthroughCopy("css");
  config.addPassthroughCopy("img");
  config.addPassthroughCopy("js");
  config.addPassthroughCopy("ftp");
  config.addPassthroughCopy("fonts");
  config.addPassthroughCopy("favicon");
  config.addPassthroughCopy("favicon.ico");

  // markdownTemplateEngine: "njk"

  return {};
};
