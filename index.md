---
layout: layouts/home.njk
title: Hi, I’m Giorgio.
link: Home
metaDesc: Giorgio Pretto. User Experience Designer.
templateEngineOverride: njk,md
tags:
  # - main_nav
---

<figure>
<img 
  id="headshot"
  src="/img/gp-shot.webp" 
  alt="Headshot of Giorgio">
</figure>

# <span>Hi,</span> <br/> <span>I'm Giorgio.</span>

I’m a **Lead UX designer** with a background in computer science and experience in product management.

I like to work on the whole spectrum of UX: **qualitative user research**, **understanding the customer needs**, and **ideate solutions**.
I ended specialise in designing expert systems in the B2B industry.

I am currently working for [2550 Engineering](https://2550.engineering/). It's a consultancy company in Gothneburg, where I have been living since 2020.

If you want to know more about me, check out my [about page](/about) or read what I am doing right [now](/now).
