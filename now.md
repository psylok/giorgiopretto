---
layout: layouts/page.njk
title: Now
link: Now
templateEngineOverride: njk,md
tags:
  - page
  - main_nav
---

# Now

As part of the [now project](https://nownownow.com) I want to share what are my current interest and activities. The plan is to keep them up to date every few months, or whenever something become obsolete.

So, what am I doing right now (December 2024)?

- After being affected by a layoff, I started my new job at [2550](https://2550.engineering/). It's a consultancy and my first job will be taking me back to where I started, working with HMI, with a bit of automotive.

- As usual I am struggling to have a good workout routine. I keep going to Taiji, and I am learning the cheng style. I have a subscription to the swimming pool and try to go a couple of times a week.
