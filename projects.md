---
layout: layouts/page.njk
title: Side Projects
link: Projects
metaDesc: Prova di testo
templateEngineOverride: njk,md
tags:
  - main_nav
---

# Side Projects

## Mirror

[Functional Mirror](/projects/mirror/)

Functional Mirror is simply what the name implies. It just fire up the camera and mirror the image, so it looks like you are looking into a mirror. I use it mostly when I exercise to check on my body position, hence the red line for reference. Useful as a generic test for a camera.
Needs permission to access camera and autoplay videos.
