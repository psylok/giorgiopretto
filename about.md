---
layout: layouts/page.njk
title: About
link: About
templateEngineOverride: njk,md
tags:
  - main_nav
  - page
---

# About

## About myself

### Work and Education

Hi. I'm an Italian Lead UX Designer living in Gothenburg, Sweden.

I started my career in UX design when I discover the field of Human Computer Interaction during my Master in Computer Science. I had the opportunity to study at RWTH Aachen in Germany, where I fell in love with all the activities related to the UX process, from field research with users to the final usability test with real people.

Being always interested in algorithms and problem solving, I realised user experience design gave me the tools to solve problems at an **higher level, based on human needs**.

I started worked as a UX consultant in Italy and ended up specializing in the B2B industry where I worked mostly with HMIs for industrial machinery. Later on I moved to Sweden where I started working in-house for SaaS companies in the renewable sector. I am now a Lead UX designer working with HMIs and automotive.
### Sport and Hobbies

I played football for all of my childhood years. In my teens I switched to futsal and altough I love it very much, the list of injuries I had make me doubt of the opposite being true.

For that reason I picked up road cycling for a couple of years when I was living in Italy, but after moving to Gothenburg, I am now trying to just go swimming like my doctor said I should, and practicing Qi Gong and taiji.

In my spare time I like to cook and bake cakes. In particular I like to make Bolognese sauce and my favourite cakes: Tiramisù and Sacher.

I like to read chinese light novels (wuxia).

## About this website

This website is often my html and CSS gym. Reason why this website changes and breaks quite often. Eventually I will reach the point were I'll stop messing around and start doing something with it. Like writing articles or something. We'll see.

Currently, I am using a static site generator, [eleventy](https://11ty.io), to generate the pages, then I commit them to [bitbucket](https://bitbucket.com) and have [netlify](https://www.netlify.com) automatically build and deploy the website.

